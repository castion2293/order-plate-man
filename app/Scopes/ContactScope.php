<?php

namespace App\Scopes;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Arr;
use Pharaoh\BaseModelRepository\Scopes\AbstractBaseScope;

class ContactScope extends AbstractBaseScope
{
    protected array $extensions = [
        'Read',
    ];

    /**
     * 篩選 已讀條件
     *
     * @param Builder $builder
     */
    protected function addRead(Builder $builder)
    {
        $builder->macro('read', function (Builder $builder, array $params) {
            // 實作過濾條件的內容部分
            $read = Arr::get($params, 'read');

            return $builder->where('read', $read);
        });
    }
}
