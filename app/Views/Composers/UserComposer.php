<?php

namespace App\Views\Composers;

use Illuminate\View\View;

class UserComposer
{
    /**
     * User Composer Model
     *
     * @param View $view
     */
    public function compose(View $view)
    {
        if (!auth()->check()) {
            return;
        }

        $view->with('user', auth()->user()->toArray());
    }
}
