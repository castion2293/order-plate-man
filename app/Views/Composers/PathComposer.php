<?php

namespace App\Views\Composers;

use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Illuminate\View\View;

class PathComposer
{
    /**
     * Page Composer
     *
     * @param View $view
     */
    public function compose(View $view)
    {
        $path = Str::of(request()->getPathInfo())
            ->after('admin/');

        $pathArray = explode('/', $path);

        $view->with(
            'path',
            [
                'title' => Arr::first($pathArray),
                'paths' => $pathArray
            ]
        );
    }
}
