<?php

namespace App\Views\Composers;

use App\Repositories\ContactRepository;
use Carbon\Carbon;
use Illuminate\View\View;

class ContactComposer
{
    private $contactRepository;

    public function __construct(ContactRepository $contactRepository)
    {
        $this->contactRepository = $contactRepository;
    }

    /**
     * User Composer Model
     *
     * @param View $view
     */
    public function compose(View $view)
    {
        $unreadContactsCount = $this->contactRepository->getUnreadContactsCount();

        $unreadContacts = $this->contactRepository->getUnreadContacts();
        $unreadContacts = collect($unreadContacts['data'])
            ->take(3)
            ->map(
                function ($contact) {
                    $contact['created_at'] = Carbon::parse($contact['created_at'])->diffForHumans();
                    return $contact;
                }
            )
            ->toArray();

        $view->with(
            'contacts',
            [
                'unread_count' => $unreadContactsCount,
                'unread' => $unreadContacts
            ]
        );
    }
}
