<?php

namespace App\Repositories;

use App\Models\Category;
use Pharaoh\BaseModelRepository\Repositories\AbstractBaseRepository;

class CategoryRepository extends AbstractBaseRepository
{
    public function __construct(Category $category)
    {
        $this->model = $category;
        $this->table = $this->model->getTable();
    }
}
