<?php

namespace App\Repositories;

use App\Models\Contact;
use App\Scopes\ContactScope;
use Pharaoh\BaseModelRepository\Repositories\AbstractBaseRepository;

class ContactRepository extends AbstractBaseRepository
{
    public function __construct(Contact $contact)
    {
        $this->model = $contact;
        $this->table = $this->model->getTable();
    }

    /**
     * 獲取所有的 contacts
     *
     * @param array $params
     * @return array
     */
    public function getContacts(array $params): array
    {
        // model scope 過濾條件方式
        $contactBuilder = $this->scopeQuery(ContactScope::class, $params);

        return $contactBuilder
            ->orderBy('id', 'desc')
            ->paginate(15)
            ->toArray();
    }

    /**
     * 獲取未讀訊息總數
     *
     * @return string
     */
    public function getUnreadContactsCount(): string
    {
        return $this->model->where('read', false)
            ->count();
    }

    /**
     * 獲取未讀訊息
     *
     * @return array
     */
    public function getUnreadContacts(): array
    {
        return $this->model->where('read', false)
            ->orderBy('id', 'desc')
            ->paginate(15)
            ->toArray();
    }
}
