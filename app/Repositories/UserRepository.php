<?php

namespace App\Repositories;

use App\Models\User;
use Pharaoh\BaseModelRepository\Repositories\AbstractBaseRepository;

class UserRepository extends AbstractBaseRepository
{
    public function __construct(User $user)
    {
        $this->model = $user;
        $this->table = $this->model->getTable();
    }
}
