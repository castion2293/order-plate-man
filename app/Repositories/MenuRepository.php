<?php

namespace App\Repositories;

use App\Models\Menu;
use Pharaoh\BaseModelRepository\Repositories\AbstractBaseRepository;

class MenuRepository extends AbstractBaseRepository
{
    public function __construct(Menu $menu)
    {
        $this->model = $menu;
        $this->table = $this->model->getTable();
    }
}
