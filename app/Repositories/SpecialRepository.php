<?php

namespace App\Repositories;

use App\Models\Special;
use Pharaoh\BaseModelRepository\Repositories\AbstractBaseRepository;

class SpecialRepository extends AbstractBaseRepository
{
    public function __construct(Special $special)
    {
        $this->model = $special;
        $this->table = $this->model->getTable();
    }
}
