<?php

namespace App\Repositories;

use App\Models\Faq;
use Pharaoh\BaseModelRepository\Repositories\AbstractBaseRepository;

class FaqRepository extends AbstractBaseRepository
{
    public function __construct(Faq $faq)
    {
        $this->model = $faq;
        $this->table = $this->model->getTable();
    }
}
