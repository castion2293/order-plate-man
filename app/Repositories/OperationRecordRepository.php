<?php

namespace App\Repositories;

use Pharaoh\BaseModelRepository\Repositories\AbstractBaseRepository;
use Pharaoh\OperationRecord\Models\OperationRecord;

class OperationRecordRepository extends AbstractBaseRepository
{
    public function __construct(OperationRecord $operationRecord)
    {
        $this->model = $operationRecord;
        $this->table = $this->model->getTable();
    }
}
