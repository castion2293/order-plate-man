<?php

namespace App\Repositories;

use App\Models\Event;
use Pharaoh\BaseModelRepository\Repositories\AbstractBaseRepository;

class EventRepository extends AbstractBaseRepository
{
    public function __construct(Event $event)
    {
        $this->model = $event;
        $this->table = $this->model->getTable();
    }
}
