<?php

namespace App\Http\Requests\Admin\User;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdateProfileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['required'],
            'company' => ['required', 'max:20'],
            'job' => ['required', 'max:20'],
            'country' => ['required', 'max:10'],
            'address' => ['required', 'max:160'],
            'phone' => ['required', 'numeric', Rule::unique('users', 'phone')->ignore($this->user()->id)],
            'email' => ['required', 'email', Rule::unique('users', 'phone')->ignore($this->user()->id)],
            'profile_img' => ['file', 'image', 'mimes:jpeg,png,jpg,gif,svg']
        ];
    }
}
