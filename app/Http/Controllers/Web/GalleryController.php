<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class GalleryController extends Controller
{
    public function __construct()
    {
    }

    public function images(Request $request)
    {
        $images = [
            'img/gallery/gallery-1.jpg',
            'img/gallery/gallery-2.jpg',
            'img/gallery/gallery-3.jpg',
            'img/gallery/gallery-4.jpg',
            'img/gallery/gallery-5.jpg',
            'img/gallery/gallery-6.jpg',
            'img/gallery/gallery-7.jpg',
            'img/gallery/gallery-8.jpg',
        ];

        $chefs = [
            [
                'image' => 'img/chefs/chefs-1.jpg',
                'name' => 'Walter White',
                'title' => 'Master Chef',
            ],
            [
                'image' => 'img/chefs/chefs-2.jpg',
                'name' => 'Sarah Jhonson',
                'title' => 'Patissier',
            ],
            [
                'image' => 'img/chefs/chefs-3.jpg',
                'name' => 'William Anderson',
                'title' => 'Cook',
            ],
        ];

        return view(
            'web.gallery',
            [
                'images' => $images,
                'chefs' => $chefs
            ]
        );
    }
}
