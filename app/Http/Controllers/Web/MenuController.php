<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Services\Web\MenuService;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

class MenuController extends Controller
{
    private $menuServices;

    public function __construct(MenuService $menuServices)
    {
        $this->menuServices = $menuServices;
    }

    /**
     * 菜單清單
     *
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function lists(Request $request)
    {
        $data = $this->menuServices->lists();

        return view(
            'web.menu',
            [
                'categories' => Arr::get($data, 'categories'),
                'items' => Arr::get($data, 'menus'),
                'specialCategories' => Arr::get($data, 'special_categories'),
                'specials' => Arr::get($data, 'specials'),
            ]
        );
    }
}
