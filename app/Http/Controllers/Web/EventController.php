<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Services\Web\EventService;
use Illuminate\Http\Request;

class EventController extends Controller
{
    private $eventService;

    public function __construct(EventService $eventService)
    {
        $this->eventService = $eventService;
    }

    public function lists(Request $request)
    {
        $data = $this->eventService->lists();

        return view(
            'web.events',
            [
                'events' => $data
            ]
        );
    }
}
