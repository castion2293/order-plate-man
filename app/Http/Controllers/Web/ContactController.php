<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Http\Requests\ContactStoreRequest;
use App\Services\Web\ContactService;
use Illuminate\Http\Request;

class ContactController extends Controller
{
    private $contactService;

    public function __construct(ContactService $contactService)
    {
        $this->contactService = $contactService;
    }

    public function index(Request $request)
    {
        return view('web.contact');
    }

    /**
     * 建立 contact
     *
     * @param ContactStoreRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(ContactStoreRequest $request)
    {
        $this->contactService->store($request->only(['name', 'phone', 'subject', 'message']));

        alert()->success('成功','謝謝你寶貴的意見，我們會儘早回覆你，感謝');

        return redirect()->back();
    }
}
