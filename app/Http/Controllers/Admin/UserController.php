<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\User\UpdatePasswordRequest;
use App\Http\Requests\Admin\User\UpdateProfileRequest;
use App\Services\Admin\UserService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    private $userService;

    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    /**
     * Display a listing of the resource.
     *
     */
    public function index(Request $request)
    {
        $user = $request->user()->toArray();

        return view(
            'admin.profile',
            [
                'user' => $user
            ]
        );
    }


    /**
     * 修改 profile 資料
     *
     * @param UpdateProfileRequest $request
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(UpdateProfileRequest $request, int $id)
    {
        $this->userService->updateProfile($id, $request->all());

        alert('修改基本資料', '成功');

        return redirect()->route('admin.user.index');
    }

    /**
     * 更換會員密碼
     *
     * @param UpdatePasswordRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function updatePassword(UpdatePasswordRequest $request)
    {
        $this->userService->updatePassword(auth()->id(), $request->all());

        Auth::logout();

        $request->session()->invalidate();

        $request->session()->regenerateToken();

        alert('修改會員密碼', '修改成功 請重新登入');

        return redirect()->route('login.index');
    }
}
