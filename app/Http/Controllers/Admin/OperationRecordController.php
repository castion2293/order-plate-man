<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Services\Admin\OperationRecordService;
use Illuminate\Http\Request;

class OperationRecordController extends Controller
{
    private $operationRecordService;

    public function __construct(OperationRecordService $operationRecordService)
    {
        $this->operationRecordService = $operationRecordService;
    }

    /**
     * 首頁 - 操作記錄
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $records = $this->operationRecordService->getUserOperationRecords($request->user());

        return response()->json($records, 200);
    }
}
