<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Services\Admin\ContactService;
use Illuminate\Http\Request;

class ContactController extends Controller
{
    private $contactService;

    public function __construct(ContactService $contactService)
    {
        $this->contactService = $contactService;
    }

    /**
     * 聯絡訊息列表
     *
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index(Request $request)
    {
        $contacts = $this->contactService->contactLists($request->all());

        return view(
            'admin.contact.contact_index',
            [
                'contacts' => $contacts
            ]
        );
    }

    /**
     * 聯絡訊息詳細
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function show(Request $request, $id)
    {
        $contact = $this->contactService->contactDetail($id);

        return view(
            'admin.contact.contact_show',
            [
                'contact' => $contact
            ]
        );
    }
}
