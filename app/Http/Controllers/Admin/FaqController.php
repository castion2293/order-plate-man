<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Services\Admin\FaqService;
use Illuminate\Http\Request;

class FaqController extends Controller
{
    private FaqService $faqService;

    public function __construct(FaqService $faqService)
    {
        $this->faqService = $faqService;
    }

    /**
     * 常見問題
     *
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index(Request $request)
    {
        $faqs = $this->faqService->faqLists();

        return view(
            'admin.faq',
            [
                'faqs' => $faqs
            ]
        );
    }
}
