<?php

namespace App\Models;

use Pharaoh\BaseModelRepository\Models\AbstractBaseModel;

class Event extends AbstractBaseModel
{
    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'items' => 'array',
    ];
}
