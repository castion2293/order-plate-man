<?php

namespace App\Models;

use App\Scopes\ContactScope;
use Pharaoh\BaseModelRepository\Models\AbstractBaseModel;

class Contact extends AbstractBaseModel
{
    /**
     * The "booted" method of the model.
     */
    protected static function booted()
    {
        static::addGlobalScope(new ContactScope);
    }

    protected $casts = [
        'read' => 'boolean'
    ];
}
