<?php

namespace App\Models;

use Pharaoh\BaseModelRepository\Models\AbstractBaseModel;

class Menu extends AbstractBaseModel
{
    public function category(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Category::class);
    }
}
