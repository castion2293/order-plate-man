<?php

namespace App\Providers;

use App\Views\Composers\ContactComposer;
use App\Views\Composers\PathComposer;
use App\Views\Composers\UserComposer;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class ViewServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        // test
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        // user data
        View::composer('admin.app', UserComposer::class);

        // page data
        View::composer('admin.app', PathComposer::class);

        // contact data
        View::composer('admin.app', ContactComposer::class);
    }
}
