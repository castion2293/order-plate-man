<?php

namespace App\Services\Admin;

use App\Repositories\OperationRecordRepository;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class OperationRecordService
{
    private $operationRecordRepository;

    public function __construct(OperationRecordRepository $operationRecordRepository)
    {
        $this->operationRecordRepository = $operationRecordRepository;
    }

    /**
     * 獲取使用者的操作記錄
     *
     * @param Model $user
     * @return array
     */
    public function getUserOperationRecords(Model $user): array
    {
        $records = $user->getOperatorRecords()
            ->orderBy('id', 'desc')
            ->limit(5)
            ->get()
            ->toArray();

        return collect($records)->map(
            function ($record) {
                $record['created_at'] = Carbon::parse($record['created_at'])->diffForHumans();
                $record['func_key'] = __("operation_record.{$record['func_key']}");
                $record['action'] = __("operation_record.{$record['action']}");
                return $record;
            }
        )->toArray();
    }
}
