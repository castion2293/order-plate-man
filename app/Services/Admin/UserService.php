<?php

namespace App\Services\Admin;

use App\Repositories\UserRepository;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;

class UserService
{
    private $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * 修改 profile 資料
     *
     * @param int $id
     * @param array $data
     */
    public function updateProfile(int $id, array $data)
    {
        DB::transaction(
            function () use ($id, $data) {
                $oldUser = $this->userRepository->find($id);

                $this->userRepository->update(
                    $id,
                    [
                        'name' => Arr::get($data, 'name'),
                        'company' => Arr::get($data, 'company'),
                        'job' => Arr::get($data, 'job'),
                        'country' => Arr::get($data, 'country'),
                        'address' => Arr::get($data, 'address'),
                        'phone' => Arr::get($data, 'phone'),
                        'email' => Arr::get($data, 'email'),
                    ]
                );

                $profileImg = Arr::get($data, 'profile_img');
                if (!empty($profileImg)) {
                    // 上傳圖片
                    $imageName = date('Ymdhis') . '.' . $profileImg->getClientOriginalExtension();
                    $profileImg->storeAs('public/profile/', $imageName);

                    $this->userRepository->update(
                        $id,
                        [
                            'profile_img' => $imageName
                        ]
                    );

                    // 刪除圖片
                    $userOldProfileImage = $oldUser->profile_img;
                    if ($userOldProfileImage !== 'profile-img.jpg') {
                        Storage::delete("public/profile/{$userOldProfileImage}");
                    }
                }

                $newUser = $this->userRepository->find($id);

                // 寫操作紀錄
                request()->user()->operating(
                    $oldUser,
                    config('operation_record.func_keys.update_profile'),
                    config('operation_record.action.update'),
                    $oldUser->toArray(),
                    $newUser->toArray(),
                    request()->ip()
                );
            }
        );
    }

    /**
     * 更換會員密碼
     *
     * @param int $id
     * @param array $data
     */
    public function updatePassword(int $id, array $data)
    {
        DB::transaction(
            function () use ($id, $data) {
                $oldUser = $this->userRepository->find($id);

                $this->userRepository->update(
                    $id,
                    [
                        'password' => Hash::make(Arr::get($data, 'new_password'))
                    ]
                );

                // 寫操作紀錄
                request()->user()->operating(
                    $oldUser,
                    config('operation_record.func_keys.update_password'),
                    config('operation_record.action.update'),
                    [],
                    [],
                    request()->ip()
                );
            }
        );
    }
}
