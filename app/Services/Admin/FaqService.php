<?php

namespace App\Services\Admin;

use App\Repositories\FaqRepository;

class FaqService
{
    private FaqRepository $faqRepository;

    public function __construct(FaqRepository $faqRepository)
    {
        $this->faqRepository = $faqRepository;
    }

    /**
     * 常見問題列表
     *
     * @return mixed
     */
    public function faqLists(): mixed
    {
        return $this->faqRepository->getAll()->toArray();
    }
}
