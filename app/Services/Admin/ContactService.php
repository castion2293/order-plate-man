<?php

namespace App\Services\Admin;

use App\Repositories\ContactRepository;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;

class ContactService
{
    private $contactRepository;

    public function __construct(ContactRepository $contactRepository)
    {
        $this->contactRepository = $contactRepository;
    }

    /**
     * 聯絡訊息列表
     *
     * @param array $params
     * @return array
     */
    public function contactLists(array $params = [])
    {
        return $this->contactRepository->getContacts($params);
    }

    /**
     * 聯絡訊息詳細
     *
     * @param int $id
     * @return mixed
     */
    public function contactDetail(int $id)
    {
        return DB::transaction(
            function () use ($id) {
                $contact = $this->contactRepository->find($id);

                // 如果是未讀改為已讀
                if (!$contact['read']) {
                    $this->contactRepository->update(
                        $id,
                        [
                            'read' => true,
                        ]
                    );

                    // 寫操作紀錄
                    request()->user()->operating(
                        $contact,
                        config('operation_record.func_keys.read_contact'),
                        config('operation_record.action.update'),
                        ['read' => false],
                        ['read' => true],
                        request()->ip()
                    );
                }

                return $contact->fresh()->toArray();
            }
        );
    }
}
