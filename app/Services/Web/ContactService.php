<?php

namespace App\Services\Web;

use App\Repositories\ContactRepository;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;

class ContactService
{
    /**
     * @var ContactRepository
     */
    private $contactRepository;

    public function __construct(ContactRepository $contactRepository)
    {
        $this->contactRepository = $contactRepository;
    }

    /**
     * 建立 contact
     *
     * @param array $params
     */
    public function store(array $params)
    {
        DB::transaction(
            function () use ($params) {
                $this->contactRepository->store(
                    [
                        'name' => Arr::get($params, 'name'),
                        'phone' => Arr::get($params, 'phone'),
                        'subject' => Arr::get($params, 'subject'),
                        'message' => Arr::get($params, 'message'),
                    ]
                );
            }
        );
    }
}
