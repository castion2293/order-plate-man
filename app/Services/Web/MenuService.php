<?php

namespace App\Services\Web;

use App\Repositories\CategoryRepository;
use App\Repositories\MenuRepository;
use App\Repositories\SpecialRepository;

class MenuService
{
    private $categoryRepository;
    private $menuRepository;
    private $specialRepository;

    public function __construct(
        CategoryRepository $categoryRepository,
        MenuRepository $menuRepository,
        SpecialRepository $specialRepository
    ) {
        $this->categoryRepository = $categoryRepository;
        $this->menuRepository = $menuRepository;
        $this->specialRepository = $specialRepository;
    }

    /**
     * 菜單清單
     */
    public function lists()
    {
        $categories = $this->categoryRepository->getAll(['name'])->pluck('name')->toArray();

        $menus = $this->menuRepository->getAll(['*'], 'category');
        $menus = $menus
            ->groupBy('en_name')
            ->map(
                function ($menuGroup) {
                    $price = $menuGroup->pluck('price')->map(
                        function ($price) {
                            return '$' . intval($price);
                        }
                    )->implode('/');

                    return [
                        'item_zh_name' => $menuGroup->first()->zh_name,
                        'item_en_name' => $menuGroup->first()->en_name,
                        'item_price' => $price,
                        'item_category' => $menuGroup->first()->category->name,
                    ];
                }
            )
            ->values()
            ->shuffle()
            ->toArray();

        $specials = $this->specialRepository->getAll();

        $specialCategories = $specials->map(
            function ($special) {
                return [
                    'tag' => $special->tag,
                    'tag_name' => $special->name
                ];
            }
        )->toArray();


        return [
            'categories' => $categories,
            'menus' => $menus,
            'special_categories' => $specialCategories,
            'specials' => $specials->toArray()
        ];
    }
}
