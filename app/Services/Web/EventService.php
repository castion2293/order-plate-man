<?php

namespace App\Services\Web;

use App\Repositories\EventRepository;

class EventService
{
    private $eventRepository;

    public function __construct(EventRepository $eventRepository)
    {
        $this->eventRepository = $eventRepository;
    }

    public function lists()
    {
        $events = $this->eventRepository->getAll();
        $events = $events->map(
            function ($event) {
                $event['price'] = '$' . number_format($event['price']);
                return $event;
            }
        )->toArray();

        return $events;
    }
}
