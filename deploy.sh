#! /bin/bash

composer dump
php artisan cache:clear
php artisan migrate:fresh --seed

# 清除 public
rm -r ./storage/app/public
rm -r ./public/storage

# 複製預設圖片
mkdir -p ./storage/app/public/profile
cp ./public/img/profile-img.jpg ./storage/app/public/profile/profile-img.jpg

php artisan storage:link
