@extends('web.app')

@section('content')
    <main id="main">
        <!-- ======= Gallery Section ======= -->
        <section id="gallery" class="gallery">
            <div class="container-fluid">

                <div class="section-title">
                    <h2><span>{{ __('web.header_name') }}</span> 的生活日常</h2>
                    <p>用相簿紀錄美食分享的每一刻</p>
                </div>

                <div class="row no-gutters">

                    @foreach($images as $image)
                        <div class="col-lg-3 col-md-4">
                            <div class="gallery-item">
                                <a href="{{ asset($image) }}" class="gallery-lightbox">
                                    <img src="{{ asset($image) }}" alt="" class="img-fluid">
                                </a>
                            </div>
                        </div>
                    @endforeach

                </div>

            </div>
        </section><!-- End Gallery Section -->

        <!-- ======= Chefs Section ======= -->
        <section id="chefs" class="chefs">
            <div class="container">

                <div class="section-title">
                    <h2><span>{{ __('web.header_name') }}</span>的大主廚們</h2>
                    <p>主廚的手藝無可挑惕</p>
                </div>

                <div class="row">

                    @foreach($chefs as $chef)
                        <div class="col-lg-4 col-md-6">
                            <div class="member">
                                <div class="pic"><img src="{{ asset($chef['image']) }}" class="img-fluid" alt=""></div>
                                <div class="member-info">
                                    <h4>{{ $chef['name'] }}</h4>
                                    <span>{{ $chef['title'] }}</span>
                                    <div class="social">
                                        <a href=""><i class="bi bi-twitter"></i></a>
                                        <a href=""><i class="bi bi-facebook"></i></a>
                                        <a href=""><i class="bi bi-instagram"></i></a>
                                        <a href=""><i class="bi bi-linkedin"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach

                </div>

            </div>
        </section><!-- End Chefs Section -->
    </main>
@endsection
