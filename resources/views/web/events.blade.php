@extends('web.app')

@section('content')
    <main id="main">
        <!-- ======= Events Section ======= -->
        <section id="events" class="events">
            <div class="container">

                <div class="section-title">
                    <h2>讓 <span>{{ __('web.header_name') }}</span> 為你辦一個特別的派對</h2>
                </div>

                <div class="events-slider swiper">
                    <div class="swiper-wrapper">

                        @foreach($events as $event)
                            <div class="swiper-slide">
                                <div class="row event-item">
                                    <div class="col-lg-6">
                                        <img src="{{ asset($event['image']) }}" class="img-fluid" alt="">
                                    </div>
                                    <div class="col-lg-6 pt-4 pt-lg-0 content">
                                        <h3>{{ $event['name'] }}</h3>
                                        <div class="price">
                                            <p><span>{{ $event['price'] }}</span></p>
                                        </div>
                                        <p class="fst-italic">
                                            {{ $event['title'] }}
                                        </p>
                                        <ul>
                                            @foreach ($event['items'] as $item)
                                                <li><i class="bi bi-check-circle"></i> {{ $item }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        @endforeach

                    </div>
                    <div class="swiper-pagination"></div>
                </div>

            </div>
        </section><!-- End Events Section -->
    </main>
@endsection
