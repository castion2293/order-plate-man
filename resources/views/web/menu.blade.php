@extends('web.app')

@section('content')
    <main id="main">
        <!-- ======= Menu Section ======= -->
        <section id="menu" class="menu">
            <div class="container">

                <div class="section-title">
                    <h2>{{ __('web.menu_title') }} <span>{{ __('web.menu') }}</span></h2>
                </div>

                <div class="row">
                    <div class="col-lg-12 d-flex justify-content-center">
                        <ul id="menu-flters">
                            <li data-filter="*" class="filter-active">{{ __('web.menu_option_all') }}</li>
                            @foreach($categories as $category)
                                <li data-filter=".filter-{{ $category }}">{{ __("web.menu_option_{$category}") }}</li>
                            @endForeach
                        </ul>
                    </div>
                </div>

                <div class="row menu-container">
                    @foreach($items as $item)
                        <div class="col-lg-6 menu-item filter-{{ $item['item_category'] }}">
                            <div class="menu-content">
                                <a href="#">{{ $item['item_zh_name'] }}</a><span>{{ $item['item_price'] }}</span>
                            </div>
                            <div class="menu-ingredients">
                                {{ $item['item_en_name'] }}
                            </div>
                        </div>
                    @endforeach
                </div>

            </div>
        </section><!-- End Menu Section -->

        <!-- ======= Specials Section ======= -->
        <section id="specials" class="specials">
            <div class="container">

                <div class="section-title">
                    <h2>{{ __('web.menu_title_2') }} <span>{{ __('web.menu_title_3') }}</span></h2>
                    <p>{{ __('web.menu_description') }}</p>
                </div>

                <div class="row">
                    <div class="col-lg-3">
                        <ul class="nav nav-tabs flex-column">
                            @foreach($specialCategories as $specialCategory)
                                <li class="nav-item">
                                    @if ($loop->first)
                                        <a class="nav-link active show" data-bs-toggle="tab" href="#{{ $specialCategory['tag'] }}">{{ $specialCategory['tag_name'] }}</a>
                                    @else
                                        <a class="nav-link" data-bs-toggle="tab" href="#{{ $specialCategory['tag'] }}">{{ $specialCategory['tag_name'] }}</a>
                                    @endif
                                </li>
                            @endforeach
                        </ul>
                    </div>
                    <div class="col-lg-9 mt-4 mt-lg-0">
                        <div class="tab-content">
                            @foreach($specials as $special)
                                @if ($loop->first)
                                    <div class="tab-pane active show" id="{{ $special['tag'] }}">
                                @else
                                    <div class="tab-pane" id="{{ $special['tag'] }}">
                                @endif
                                        <div class="row">
                                            <div class="col-lg-8 details order-2 order-lg-1">
                                                <h3>{{ $special['name'] }}</h3>
                                                <p class="fst-italic">{{ $special['title'] }}</p>
                                                <p>{{ $special['description'] }}</p>
                                            </div>
                                            <div class="col-lg-4 text-center order-1 order-lg-2">
                                                <img src="{{ asset($special['image']) }}" alt="" class="img-fluid">
                                            </div>
                                        </div>
                                    </div>
                            @endforeach
                        </div>
                    </div>
                </div>

            </div>
        </section><!-- End Specials Section -->
    </main>
@endsection
