@extends('web.app')

@section('content')
    <main id="main">
        <!-- ======= About Section ======= -->
        <section id="about" class="about">
            <div class="container-fluid">

                <div class="row">

                    <div class="col-lg-5 align-items-stretch video-box" style='background-image: url("{{ asset('img/popbar_1.jpeg') }}");'>
                        <a href="https://www.youtube.com/watch?v=XoexhUOpXSI&ab_channel=WTVIPBSCLT" class="venobox play-btn mb-4" data-vbtype="video" data-autoplay="true"></a>
                    </div>

                    <div class="col-lg-7 d-flex flex-column justify-content-center align-items-stretch">

                        <div class="content">
                            <h3>{{ __('web.about_header_1') }} <strong>{{ __('web.about_header_2') }}</strong></h3>
                            <p>{{ __('web.about_description_1') }}</p>
                            <p class="fst-italic">{{ __('web.about_description_2') }}</p>
                            <ul>
                                <li><i class="bx bx-check-double"></i>{{ __('web.about_list_1') }}</li>
                                <li><i class="bx bx-check-double"></i>{{ __('web.about_list_2') }}</li>
                                <li><i class="bx bx-check-double"></i>{{ __('web.about_list_3') }}</li>
                                <li><i class="bx bx-check-double"></i>{{ __('web.about_list_4') }}</li>
                                <li><i class="bx bx-check-double"></i>{{ __('web.about_list_5') }}</li>
                                <li><i class="bx bx-check-double"></i>{{ __('web.about_list_6') }}</li>
                                <li><i class="bx bx-check-double"></i>{{ __('web.about_list_7') }}</li>
                            </ul>
                        </div>

                    </div>

                </div>

            </div>
        </section><!-- End About Section -->

        <!-- ======= Whu Us Section ======= -->
        <section id="why-us" class="why-us">
            <div class="container">

                <div class="section-title">
                    <h2>{{ __('web.about_header_3') }} <span>{{ __('web.header_name') }}</span></h2>
                    <p>{{ __('web.about_description_3') }}</p>
                </div>

                <div class="row">

                    <div class="col-lg-4">
                        <div class="box">
                            <span>01</span>
                            <h4>{{ __('web.about_card_title_1') }}</h4>
                            <p>{{ __('web.about_card_description_1') }}</p>
                        </div>
                    </div>

                    <div class="col-lg-4 mt-4 mt-lg-0">
                        <div class="box">
                            <span>02</span>
                            <h4>{{ __('web.about_card_title_2') }}</h4>
                            <p>{{ __('web.about_card_description_2') }}</p>
                        </div>
                    </div>

                    <div class="col-lg-4 mt-4 mt-lg-0">
                        <div class="box">
                            <span>03</span>
                            <h4>{{ __('web.about_card_title_3') }}</h4>
                            <p>{{ __('web.about_card_description_3') }}</p>
                        </div>
                    </div>

                </div>

            </div>
        </section><!-- End Whu Us Section -->
    </main>
@endsection
