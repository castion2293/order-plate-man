@extends('web.app')

@section('content')
    <!-- ======= Hero Section ======= -->
    <section id="hero">
        <div class="hero-container">
            <div id="heroCarousel" data-bs-interval="5000" class="carousel slide carousel-fade" data-bs-ride="carousel">

                <ol class="carousel-indicators" id="hero-carousel-indicators"></ol>

                <div class="carousel-inner" role="listbox">

                    <!-- Slide 1 -->
                    <div class="carousel-item active" style="background: url('{{ asset('img/slide/slide-1.jpg') }}');">
                        <div class="carousel-container">
                            <div class="carousel-content">
                                <h2 class="animate__animated animate__fadeInDown"><span>{{ __('web.header_name') }}</span> 炸物專賣快餐</h2>
                                <p class="animate__animated animate__fadeInUp">{{ __('web.slider_description_1') }}</p>
                                <div>
                                    <a href="{{ route('menu') }}" class="btn-menu animate__animated animate__fadeInUp scrollto">{{ __('web.menu') }}</a>
                                    <a href="#book-a-table" class="btn-book animate__animated animate__fadeInUp scrollto">{{ __('web.ordering') }}</a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Slide 2 -->
                    <div class="carousel-item" style="background: url('{{ asset('img/slide/slide-2.jpg') }}');">
                        <div class="carousel-container">
                            <div class="carousel-content">
                                <h2 class="animate__animated animate__fadeInDown">{{ __('web.slider_header_1') }}</h2>
                                <p class="animate__animated animate__fadeInUp">{{ __('web.slider_description_2') }}</p>
                                <div>
                                    <a href="{{ route('menu') }}" class="btn-menu animate__animated animate__fadeInUp scrollto">{{ __('web.menu') }}</a>
                                    <a href="#book-a-table" class="btn-book animate__animated animate__fadeInUp scrollto">{{ __('web.ordering') }}</a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Slide 3 -->
                    <div class="carousel-item" style="background: url('{{ asset('img/slide/slide-3.jpg') }}');">
                        <div class="carousel-background"><img src="{{ asset('img/slide/slide-3.jpg') }}" alt=""></div>
                        <div class="carousel-container">
                            <div class="carousel-content">
                                <h2 class="animate__animated animate__fadeInDown">{{ __('web.slider_header_3') }}</h2>
                                <p class="animate__animated animate__fadeInUp">{{ __('web.slider_description_3') }}</p>
                                <div>
                                    <a href="{{ route('menu') }}" class="btn-menu animate__animated animate__fadeInUp scrollto">{{ __('web.menu') }}</a>
                                    <a href="#book-a-table" class="btn-book animate__animated animate__fadeInUp scrollto">{{ __('web.ordering') }}</a>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

                <a class="carousel-control-prev" href="#heroCarousel" role="button" data-bs-slide="prev">
                    <span class="carousel-control-prev-icon bi bi-chevron-left" aria-hidden="true"></span>
                </a>

                <a class="carousel-control-next" href="#heroCarousel" role="button" data-bs-slide="next">
                    <span class="carousel-control-next-icon bi bi-chevron-right" aria-hidden="true"></span>
                </a>

            </div>
        </div>
    </section><!-- End Hero -->
@endsection
