@extends('admin.app')

@section('content')
    <section class="section">
        <!-- F.A.Q Group 2 -->
        <div class="card">
            <div class="card-body">
                <h5 class="card-title">常見問題</h5>

                <div class="accordion accordion-flush" id="faq-group-2">

                    @foreach($faqs as $faq)
                        <div class="accordion-item">
                            <h2 class="accordion-header">
                                <button class="accordion-button collapsed" data-bs-target="#faqsTwo-{{ $faq['id'] }}" type="button" data-bs-toggle="collapse">
                                    {{ $faq['title'] }}
                                </button>
                            </h2>
                            <div id="faqsTwo-{{ $faq['id'] }}" class="accordion-collapse collapse" data-bs-parent="#faq-group-2">
                                <div class="accordion-body">
                                    {{ $faq['content'] }}
                                </div>
                            </div>
                        </div>
                    @endforeach

                </div>

            </div>
        </div><!-- End F.A.Q Group 2 -->
    </section>
@endsection
