<!-- Customers Card -->
<div class="col-xxl-4 col-xl-12" x-data="customers">

    <div class="card info-card customers-card">

        <div class="filter">
            <a class="icon" href="#" data-bs-toggle="dropdown"><i class="bi bi-three-dots"></i></a>
            <ul class="dropdown-menu dropdown-menu-end dropdown-menu-arrow">
                <li><a class="dropdown-item hover-pointer" @click="changeRange('today')">今天</a></li>
                <li><a class="dropdown-item hover-pointer" @click="changeRange('week')">本週</a></li>
                <li><a class="dropdown-item hover-pointer" @click="changeRange('month')">本月</a></li>
            </ul>
        </div>

        <div class="card-body">
            <h5 class="card-title">顧客量 <span x-text="'| ' + rangeName"></span></h5>

            <div class="d-flex align-items-center">
                <div
                    class="card-icon rounded-circle d-flex align-items-center justify-content-center">
                    <i class="bi bi-people"></i>
                </div>
                <div class="ps-3">
                    <h6 x-text="customers[range]"></h6>
                    <span class="text-danger small pt-1 fw-bold">12%</span> <span
                        class="text-muted small pt-2 ps-1">decrease</span>

                </div>
            </div>

        </div>
    </div>

</div><!-- End Customers Card -->

<script>
    document.addEventListener('alpine:init', () => {
        Alpine.data('customers', () => ({
            rangeItems: {
                'today': '今天',
                'week': '本週',
                'month': '本月',
            },

            range: '',
            rangeName: '',

            customers: [],

            init () {
                this.range = 'today'
                this.rangeName = this.rangeItems['today']

                this.initialCustomers()
            },

            changeRange (range) {
                this.range = range
                this.rangeName = this.rangeItems[range]
            },

            async initialCustomers () {
                this.customers = await fetch('{{ route('api.admin.home.customers') }}')
                    .then(response => response.json())
            },
        }))
    })
</script>
