<!-- Reports -->
<div class="col-12" x-data="reports">
    <div class="card">

        <div class="filter">
            <a class="icon" href="#" data-bs-toggle="dropdown"><i class="bi bi-three-dots"></i></a>
            <ul class="dropdown-menu dropdown-menu-end dropdown-menu-arrow">
                <li><a class="dropdown-item hover-pointer" @click="changeRange('today')">今天</a></li>
                <li><a class="dropdown-item hover-pointer" @click="changeRange('week')">本週</a></li>
                <li><a class="dropdown-item hover-pointer" @click="changeRange('month')">本月</a></li>
            </ul>
        </div>

        <div class="card-body">
            <h5 class="card-title">報表 <span x-text="'| ' + rangeName"></span></h5>

            <!-- Line Chart -->
            <div id="reportsChart"></div>
            <!-- End Line Chart -->

        </div>

    </div>
</div><!-- End Reports -->

<script>
    document.addEventListener('alpine:init', () => {
        Alpine.data('reports', () => ({
            rangeItems: {
                'today': '今天',
                'week': '本週',
                'month': '本月',
            },

            range: '',
            rangeName: '',

            chart: '',

            url: '{{ route('api.admin.home.reports') }}',

            init () {
                this.range = 'today'
                this.rangeName = this.rangeItems['today']

                this.fetchReports(this.range)
            },

            changeRange (range) {
                this.range = range
                this.rangeName = this.rangeItems[range]

                this.changeReports(this.range)
            },

            async fetchReports () {
                let reportData = await fetch(this.url)
                    .then(response => response.json())

                this.chart = new ApexCharts(document.querySelector("#reportsChart"), {
                    series: reportData['series'],
                    chart: {
                        height: 350,
                        type: 'area',
                        toolbar: {
                            show: false
                        },
                    },
                    markers: {
                        size: 4
                    },
                    colors: ['#4154f1', '#2eca6a', '#ff771d'],
                    fill: {
                        type: "gradient",
                        gradient: {
                            shadeIntensity: 1,
                            opacityFrom: 0.3,
                            opacityTo: 0.4,
                            stops: [0, 90, 100]
                        }
                    },
                    dataLabels: {
                        enabled: false
                    },
                    stroke: {
                        curve: 'smooth',
                        width: 2
                    },
                    xaxis: {
                        type: 'datetime',
                        categories: reportData['categories'],
                        labels: {
                            datetimeUTC: false
                        }
                    },
                    tooltip: {
                        x: {
                            format: 'dd/MM/yy HH:mm'
                        },
                    }
                })

                this.chart.render()
            },

            async changeReports (range) {
                let reportData = await fetch(this.url)
                    .then(response => response.json())

                this.chart.updateOptions({
                    series: reportData['series'],
                    xaxis: {
                        categories: reportData['categories'],
                    }
                })
            }
        }))
    })
</script>
