<!-- Sales Card -->
<div class="col-xxl-4 col-md-6" x-data="sales">
    <div class="card info-card sales-card">

        <div class="filter">
            <a class="icon" href="#" data-bs-toggle="dropdown"><i class="bi bi-three-dots"></i></a>
            <ul class="dropdown-menu dropdown-menu-end dropdown-menu-arrow">
                <li><a class="dropdown-item hover-pointer" @click="changeRange('today')">今天</a></li>
                <li><a class="dropdown-item hover-pointer" @click="changeRange('week')">本週</a></li>
                <li><a class="dropdown-item hover-pointer" @click="changeRange('month')">本月</a></li>
            </ul>
        </div>

        <div class="card-body">
            <h5 class="card-title">銷售量 <span x-text="'| ' + rangeName"></span></h5>

            <div class="d-flex align-items-center">
                <div
                    class="card-icon rounded-circle d-flex align-items-center justify-content-center">
                    <i class="bi bi-cart"></i>
                </div>
                <div class="ps-3">
                    <h6 x-text="sales[range]"></h6>
                    <span class="text-success small pt-1 fw-bold">12%</span> <span
                        class="text-muted small pt-2 ps-1">increase</span>

                </div>
            </div>
        </div>

    </div>
</div><!-- End Sales Card -->

<script>
    document.addEventListener('alpine:init', () => {
        Alpine.data('sales', () => ({
            rangeItems: {
                'today': '今天',
                'week': '本週',
                'month': '本月',
            },

            range: '',
            rangeName: '',

            sales: [],

            init () {
                this.range = 'today'
                this.rangeName = this.rangeItems['today']

                this.initialSales()
            },

            changeRange (range) {
                this.range = range
                this.rangeName = this.rangeItems[range]
            },

            async initialSales () {
                this.sales = await fetch('{{ route('api.admin.home.sales') }}')
                    .then(response => response.json())
            },
        }))
    })
</script>
