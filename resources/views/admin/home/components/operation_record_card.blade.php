<!-- Recent Activity -->
<div class="card" x-data="operation_record">
    {{--                    <div class="filter">--}}
    {{--                        <a class="icon" href="#" data-bs-toggle="dropdown"><i class="bi bi-three-dots"></i></a>--}}
    {{--                        <ul class="dropdown-menu dropdown-menu-end dropdown-menu-arrow">--}}
    {{--                            <li class="dropdown-header text-start">--}}
    {{--                                <h6>Filter</h6>--}}
    {{--                            </li>--}}

    {{--                            <li><a class="dropdown-item" href="#">Today</a></li>--}}
    {{--                            <li><a class="dropdown-item" href="#">This Month</a></li>--}}
    {{--                            <li><a class="dropdown-item" href="#">This Year</a></li>--}}
    {{--                        </ul>--}}
    {{--                    </div>--}}

    <div class="card-body">
        <h5 class="card-title">操作紀錄</h5>

        <div class="activity">

            <template x-for="operation_record in operation_records">
                <div class="activity-item d-flex">
                    <div class="activite-label" x-text="operation_record['created_at']"></div>
                    <i class='bi bi-circle-fill activity-badge align-self-start' :class="operation_record['tag']"></i>
                    <div class="activity-content">
                        <span x-text="operation_record['action']"></span>
                        <span x-text="operation_record['func_key']" class="fw-bold text-dark"></span>
                    </div>
                </div><!-- End activity item-->
            </template>

        </div>

    </div>
</div><!-- End Recent Activity -->

<script>
    document.addEventListener('alpine:init', () => {
        Alpine.data('operation_record', () => ({
            operation_records: [],

            operation_tags: ['text-success', 'text-danger', 'text-primary', 'text-info', 'text-warning', 'text-muted'],

            init () {
                this.initialOperationRecords()
            },

            async initialOperationRecords () {
                this.operation_records = await fetch('{{ route('api.admin.home.operation_record') }}')
                    .then(response => response.json())

                this.operation_records = _.map(this.operation_records, ((record) => {
                    return {
                        'action': record['action'],
                        'func_key': record['func_key'],
                        'created_at': record['created_at'],
                        'tag': _.takeRight(_.shuffle(this.operation_tags))
                    }
                }))
            },

        }))
    })
</script>
