<!-- Revenue Card -->
<div class="col-xxl-4 col-md-6" x-data="revenues">
    <div class="card info-card revenue-card">

        <div class="filter">
            <a class="icon" href="#" data-bs-toggle="dropdown"><i class="bi bi-three-dots"></i></a>
            <ul class="dropdown-menu dropdown-menu-end dropdown-menu-arrow">
                <li><a class="dropdown-item hover-pointer" @click="changeRange('today')">今天</a></li>
                <li><a class="dropdown-item hover-pointer" @click="changeRange('week')">本週</a></li>
                <li><a class="dropdown-item hover-pointer" @click="changeRange('month')">本月</a></li>
            </ul>
        </div>

        <div class="card-body">
            <h5 class="card-title">營業額 <span x-text="'| ' + rangeName"></span></h5>

            <div class="d-flex align-items-center">
                <div
                    class="card-icon rounded-circle d-flex align-items-center justify-content-center">
                    <i class="bi bi-currency-dollar"></i>
                </div>
                <div class="ps-3">
                    <h6 x-text="'$' + revenues[range].toLocaleString()"></h6>
                    <span class="text-success small pt-1 fw-bold">8%</span> <span
                        class="text-muted small pt-2 ps-1">increase</span>

                </div>
            </div>
        </div>

    </div>
</div><!-- End Revenue Card -->

<script>
    document.addEventListener('alpine:init', () => {
        Alpine.data('revenues', () => ({
            rangeItems: {
                'today': '今天',
                'week': '本週',
                'month': '本月',
            },

            range: '',
            rangeName: '',

            // revenues: [],
            revenues: {
                'today': ''
            },

            init () {
                this.range = 'today'
                this.rangeName = this.rangeItems['today']

                this.initialRevenues()
            },

            changeRange (range) {
                this.range = range
                this.rangeName = this.rangeItems[range]
            },

            async initialRevenues () {
                this.revenues = await fetch('{{ route('api.admin.home.revenues') }}')
                    .then(response => response.json())
            },
        }))
    })
</script>
