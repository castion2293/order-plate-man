@extends('admin.app')

@section('content')
    <section class="section">
        <div class="card">
            <div class="card-body">
                <h5 class="card-title">聯絡訊息</h5>
                <!-- Bordered Table -->
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th scope="col" class="text-center">姓名</th>
                        <th scope="col" class="text-center">標題</th>
                        <th scope="col" class="text-center">時間</th>
                        <th scope="col" class="text-center">開啟</th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach($contacts['data'] as $contact)
                            <tr>
                                <td>{{ $contact['name'] }}</td>
                                <td>{{ Str::limit($contact['subject'], 40) }}</td>
                                <td class="text-center">{{ $contact['created_at'] }}</td>
                                <td class="text-center">
                                    <a href="{{ route('admin.contact.show', ['id' => $contact['id']]) }}">
                                        @if ($contact['read'])
                                            <i class="bi bi-eye text-black"></i>
                                        @else ($contact['read')
                                            <i class="bi bi-eye"></i>
                                        @endif
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                <!-- End Bordered Table -->

                <!-- Pagination with icons -->
                    <nav aria-label="Page navigation example">
                        <ul class="pagination justify-content-center">
                            <li class="page-item">
                                <a class="page-link" href="{{ $contacts['first_page_url'] }}" aria-label="Previous">
                                    <span aria-hidden="true">&laquo;</span>
                                </a>
                            </li>
                            <li class="page-item">
                                <a class="page-link" href="{{ $contacts['prev_page_url'] }}" aria-label="Previous">
                                    <span aria-hidden="true">&lt;</span>
                                </a>
                            </li>
                            <li class="page-item">
                                <a class="page-link" href="{{ $contacts['next_page_url'] }}" aria-label="Previous">
                                    <span aria-hidden="true">&gt;</span>
                                </a>
                            </li>
                            <li class="page-item">
                                <a class="page-link" href="{{ $contacts['last_page_url'] }}" aria-label="Next">
                                    <span aria-hidden="true">&raquo;</span>
                                </a>
                            </li>
                        </ul>
                    </nav>
                <!-- End Pagination with icons -->

            </div>
        </div>
    </section>
@endsection
