@extends('admin.app')

@section('content')
    <section class="section">
        <!-- Card with titles, buttons, and links -->
        <div class="card">
            <div class="card-body">
                <h5 class="card-title">{{ $contact['subject'] }}</h5>
                <h6 class="card-subtitle mb-2 text-muted">從 <strong class="text-black">{{ $contact['name'] }}</strong> 發送</h6>
                <h6 class="card-subtitle mb-2 text-muted">電話: <strong class="text-black">{{ $contact['phone'] }}</strong></h6>
                <br>
                <p class="card-text">{{ $contact['message'] }}</p>
            </div>
        </div><!-- End Card with titles, buttons, and links -->
    </section>
@endsection
