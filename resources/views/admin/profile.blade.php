@extends('admin.app')

@section('content')
    <section class="section profile">
        <div class="row">
            <div class="col-xl-4">

                <div class="card">
                    <div class="card-body profile-card pt-4 d-flex flex-column align-items-center">

                        <img src="{{ asset("storage/profile/{$user['profile_img']}") }}" alt="Profile" class="rounded-circle">
                        <h2>{{ $user['name'] }}</h2>
                        <h3>{{ $user['email'] }}</h3>
                        <div class="social-links mt-2">
                            <a href="#" class="twitter"><i class="bi bi-twitter"></i></a>
                            <a href="#" class="facebook"><i class="bi bi-facebook"></i></a>
                            <a href="#" class="instagram"><i class="bi bi-instagram"></i></a>
                            <a href="#" class="linkedin"><i class="bi bi-linkedin"></i></a>
                        </div>
                    </div>
                </div>

            </div>

            <div class="col-xl-8">

                <div class="card">
                    <div x-data="navLink" class="card-body pt-3">
                        <!-- Bordered Tabs -->
                        <ul class="nav nav-tabs nav-tabs-bordered">
                            <template x-for="(item, index) in items" :key="index">
                                <li class="nav-item">
                                    <button class="nav-link"
                                            :class="(nav_link === item['tab']) ? 'active' : ''"
                                            data-bs-toggle="tab" :data-bs-target="'#' + item['tab']"
                                            x-text="item['name']"></button>
                                </li>
                            </template>
                        </ul>
                        <div class="tab-content pt-2">

                            <div class="tab-pane fade profile-overview" :class="(nav_link === 'profile-overview') ? 'active show' : ''" id="profile-overview">
                                {{--                                <h5 class="card-title">About</h5>--}}
                                {{--                                <p class="small fst-italic">Sunt est soluta temporibus accusantium neque nam maiores cumque temporibus. Tempora libero non est unde veniam est qui dolor. Ut sunt iure rerum quae quisquam autem eveniet perspiciatis odit. Fuga sequi sed ea saepe at unde.</p>--}}

                                <h5 class="card-title">個人詳細</h5>

                                <div class="row">
                                    <div class="col-lg-3 col-md-4 label ">姓名</div>
                                    <div class="col-lg-9 col-md-8">{{ $user['name'] }}</div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-3 col-md-4 label">公司</div>
                                    <div class="col-lg-9 col-md-8">{{ $user['company'] }}</div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-3 col-md-4 label">職位</div>
                                    <div class="col-lg-9 col-md-8">{{ $user['job'] }}</div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-3 col-md-4 label">國家</div>
                                    <div class="col-lg-9 col-md-8">{{ $user['country'] }}</div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-3 col-md-4 label">地址</div>
                                    <div class="col-lg-9 col-md-8">{{ $user['address'] }}</div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-3 col-md-4 label">電話</div>
                                    <div class="col-lg-9 col-md-8">{{ $user['phone'] }}</div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-3 col-md-4 label">Email</div>
                                    <div class="col-lg-9 col-md-8">{{ $user['email'] }}</div>
                                </div>

                            </div>

                            <div class="tab-pane fade profile-edit pt-3" :class="(nav_link === 'profile-edit') ? 'active show' : ''" id="profile-edit">

                                <!-- Profile Edit Form -->
                                <form action="{{ route('admin.user.update', ['user' => $user['id']]) }}" method="post" enctype="multipart/form-data">
                                    @method('put')
                                    @csrf
                                    <input hidden name="post_field" value="profile-edit">

                                    <div class="row mb-3">
                                        <label for="profileImage" class="col-md-4 col-lg-3 col-form-label">個人頭像</label>
                                        <div class="col-md-8 col-lg-9">
                                            <img :src="image_url" alt="Profile">
                                            <div class="pt-2">
                                                <a class="btn btn-primary btn-sm"
                                                   title="Upload new profile image"
                                                   @click="$refs.select_file.click()"
                                                ><i class="bi bi-upload"></i></a>
                                                <a href="#" class="btn btn-danger btn-sm"
                                                   title="Remove my profile image"><i class="bi bi-trash"></i></a>

                                                <!-- Image file selector -->
                                                <input type="file" accept="image/*" name="profile_img" hidden x-ref="select_file" @change="imageChange(event)">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row mb-3">
                                        <label for="fullName" class="col-md-4 col-lg-3 col-form-label">姓名</label>
                                        <div class="col-md-8 col-lg-9">
                                            <input name="name" type="text" class="form-control" id="fullName"
                                                   value="{{ $user['name'] ?? old('name') }}">
                                        </div>
                                    </div>

                                    {{--                                    <div class="row mb-3">--}}
                                    {{--                                        <label for="about" class="col-md-4 col-lg-3 col-form-label">About</label>--}}
                                    {{--                                        <div class="col-md-8 col-lg-9">--}}
                                    {{--                                            <textarea name="about" class="form-control" id="about" style="height: 100px">Sunt est soluta temporibus accusantium neque nam maiores cumque temporibus. Tempora libero non est unde veniam est qui dolor. Ut sunt iure rerum quae quisquam autem eveniet perspiciatis odit. Fuga sequi sed ea saepe at unde.</textarea>--}}
                                    {{--                                        </div>--}}
                                    {{--                                    </div>--}}

                                    <div class="row mb-3">
                                        <label for="company" class="col-md-4 col-lg-3 col-form-label">公司</label>
                                        <div class="col-md-8 col-lg-9">
                                            <input name="company" type="text" class="form-control" id="company"
                                                   value="{{ $user['company'] ?? old('company') }}">
                                        </div>
                                    </div>

                                    <div class="row mb-3">
                                        <label for="Job" class="col-md-4 col-lg-3 col-form-label">職位</label>
                                        <div class="col-md-8 col-lg-9">
                                            <input name="job" type="text" class="form-control" id="Job"
                                                   value="{{ $user['job'] ?? old('job') }}">
                                        </div>
                                    </div>

                                    <div class="row mb-3">
                                        <label for="Country" class="col-md-4 col-lg-3 col-form-label">國家</label>
                                        <div class="col-md-8 col-lg-9">
                                            <input name="country" type="text" class="form-control" id="Country"
                                                   value="{{ $user['country'] ?? old('country') }}">
                                        </div>
                                    </div>

                                    <div class="row mb-3">
                                        <label for="Address" class="col-md-4 col-lg-3 col-form-label">地址</label>
                                        <div class="col-md-8 col-lg-9">
                                            <input name="address" type="text" class="form-control" id="Address"
                                                   value="{{ $user['address'] ?? old('address') }}">
                                        </div>
                                    </div>

                                    <div class="row mb-3">
                                        <label for="Phone" class="col-md-4 col-lg-3 col-form-label">電話</label>
                                        <div class="col-md-8 col-lg-9">
                                            <input name="phone" type="text" class="form-control" id="Phone"
                                                   value="{{ $user['phone'] ?? old('phone') }}">
                                        </div>
                                    </div>

                                    <div class="row mb-3">
                                        <label for="Email" class="col-md-4 col-lg-3 col-form-label">Email</label>
                                        <div class="col-md-8 col-lg-9">
                                            <input name="email" type="email" class="form-control" id="Email"
                                                   value="{{ $user['email'] ?? old('email') }}">
                                        </div>
                                    </div>

                                    {{--                                    <div class="row mb-3">--}}
                                    {{--                                        <label for="Twitter" class="col-md-4 col-lg-3 col-form-label">Twitter Profile</label>--}}
                                    {{--                                        <div class="col-md-8 col-lg-9">--}}
                                    {{--                                            <input name="twitter" type="text" class="form-control" id="Twitter" value="https://twitter.com/#">--}}
                                    {{--                                        </div>--}}
                                    {{--                                    </div>--}}

                                    {{--                                    <div class="row mb-3">--}}
                                    {{--                                        <label for="Facebook" class="col-md-4 col-lg-3 col-form-label">Facebook Profile</label>--}}
                                    {{--                                        <div class="col-md-8 col-lg-9">--}}
                                    {{--                                            <input name="facebook" type="text" class="form-control" id="Facebook" value="https://facebook.com/#">--}}
                                    {{--                                        </div>--}}
                                    {{--                                    </div>--}}

                                    {{--                                    <div class="row mb-3">--}}
                                    {{--                                        <label for="Instagram" class="col-md-4 col-lg-3 col-form-label">Instagram Profile</label>--}}
                                    {{--                                        <div class="col-md-8 col-lg-9">--}}
                                    {{--                                            <input name="instagram" type="text" class="form-control" id="Instagram" value="https://instagram.com/#">--}}
                                    {{--                                        </div>--}}
                                    {{--                                    </div>--}}

                                    {{--                                    <div class="row mb-3">--}}
                                    {{--                                        <label for="Linkedin" class="col-md-4 col-lg-3 col-form-label">Linkedin Profile</label>--}}
                                    {{--                                        <div class="col-md-8 col-lg-9">--}}
                                    {{--                                            <input name="linkedin" type="text" class="form-control" id="Linkedin" value="https://linkedin.com/#">--}}
                                    {{--                                        </div>--}}
                                    {{--                                    </div>--}}

                                    <div class="text-center">
                                        <button type="submit" class="btn btn-primary">儲存</button>
                                    </div>
                                </form><!-- End Profile Edit Form -->

                            </div>

                            <div class="tab-pane fade pt-3" :class="(nav_link === 'profile-settings') ? 'active show' : ''" id="profile-settings">

                                <!-- Settings Form -->
                                <form>

                                    <div class="row mb-3">
                                        <label for="fullName" class="col-md-4 col-lg-3 col-form-label">Email
                                            Notifications</label>
                                        <div class="col-md-8 col-lg-9">
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" id="changesMade"
                                                       checked>
                                                <label class="form-check-label" for="changesMade">
                                                    Changes made to your account
                                                </label>
                                            </div>
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" id="newProducts"
                                                       checked>
                                                <label class="form-check-label" for="newProducts">
                                                    Information on new products and services
                                                </label>
                                            </div>
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" id="proOffers">
                                                <label class="form-check-label" for="proOffers">
                                                    Marketing and promo offers
                                                </label>
                                            </div>
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" id="securityNotify"
                                                       checked disabled>
                                                <label class="form-check-label" for="securityNotify">
                                                    Security alerts
                                                </label>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="text-center">
                                        <button type="submit" class="btn btn-primary">Save Changes</button>
                                    </div>
                                </form><!-- End settings Form -->

                            </div>

                            <div class="tab-pane fade pt-3" :class="(nav_link === 'profile-change-password') ? 'active show' : ''" id="profile-change-password">
                                <!-- Change Password Form -->
                                <form action="{{ route('admin.user.update_password') }}" method="post" enctype="multipart/form-data">
                                    @csrf
                                    @method('put')
                                    <input hidden name="post_field" value="profile-change-password">
                                    <div class="row mb-3">
                                        <label for="currentPassword" class="col-md-4 col-lg-3 col-form-label">目前密碼</label>
                                        <div class="col-md-8 col-lg-9">
                                            <input name="password" type="password" class="form-control"
                                                   id="currentPassword">
                                        </div>
                                    </div>

                                    <div class="row mb-3">
                                        <label for="newPassword" class="col-md-4 col-lg-3 col-form-label">新密碼</label>
                                        <div class="col-md-8 col-lg-9">
                                            <input name="new_password" type="password" class="form-control"
                                                   id="newPassword">
                                        </div>
                                    </div>

                                    <div class="row mb-3">
                                        <label for="renewPassword" class="col-md-4 col-lg-3 col-form-label">確認密碼</label>
                                        <div class="col-md-8 col-lg-9">
                                            <input name="new_password_confirmation" type="password" class="form-control"
                                                   id="renewPassword">
                                        </div>
                                    </div>

                                    <div class="text-center">
                                        <button type="submit" class="btn btn-primary">更換密碼</button>
                                    </div>
                                </form><!-- End Change Password Form -->

                            </div>

                        </div><!-- End Bordered Tabs -->

                    </div>
                </div>

            </div>
        </div>
    </section>

    <script>
        document.addEventListener('alpine:init', () => {
            Alpine.data('navLink', () => ({
                items: [
                    {
                        'tab': 'profile-overview',
                        'name': '簡介'
                    },
                    {
                        'tab': 'profile-edit',
                        'name': '修改'
                    },
                    {
                        'tab': 'profile-settings',
                        'name': '設定'
                    },
                    {
                        'tab': 'profile-change-password',
                        'name': '更換密碼'
                    }
                ],

                nav_link: '',

                image_url: '',

                init () {
                    this.nav_link = 'profile-overview'

                    let post_field = '{{ session()->get('_old_input.post_field') }}'

                    if (post_field !== '') {
                        this.nav_link = post_field
                    }

                    this.image_url = '{{ asset("storage/profile/{$user['profile_img']}") }}'
                },

                imageChange () {
                    reader = new FileReader()
                    file = event.target.files[0]
                    reader.onload = (event) => {
                        this.image_url = event.target.result
                    }
                    reader.readAsDataURL(file)
                }
            }))
        })
    </script>
@endsection
