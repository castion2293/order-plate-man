<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id')->comment('PK');
            $table->string('name')->comment('名稱');
            $table->string('email')->unique()->comment('Email');
            $table->string('phone')->unique()->comment('電話');
            $table->string('password')->comment('密碼');
            $table->string('profile_img')->comment('頭像');
            $table->string('company', 20)->comment('公司');
            $table->string('job', 20)->comment('職位');
            $table->string('country', 10)->comment('國家');
            $table->string('address')->comment('地址');
            $table->rememberToken();

            // 建立時間
            $table->datetime('created_at')
                ->default(DB::raw('CURRENT_TIMESTAMP'))
                ->comment('建立時間');

            // 最後更新
            $table->datetime('updated_at')
                ->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'))
                ->comment('最後更新');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
