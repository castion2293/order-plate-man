<?php

namespace Database\Seeders;

use App\Models\category;
use App\Repositories\CategoryRepository;
use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function run()
    {
        $categoryRepository = app()->make(CategoryRepository::class);
        $categories = collect(config('default.categories'))->map(
            function ($category) {
                return [
                    'name' => $category
                ];
            }
        )->toArray();

        $categoryRepository->insertMuti($categories);

        $this->command->getOutput()->writeln( get_class($this) . '種子建立成功');
    }
}
