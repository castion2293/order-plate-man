<?php

namespace Database\Seeders;

use App\Repositories\CategoryRepository;
use App\Repositories\MenuRepository;
use Illuminate\Database\Seeder;
use Illuminate\Support\Arr;

class MenuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function run()
    {
        $menuRepository = app()->make(MenuRepository::class);
        $categoryRepository = app()->make(CategoryRepository::class);

        $categories = $categoryRepository->getAll(['id', 'name']);
        $categories = collect($categories)->mapWithKeys(
            function ($category) {
                return [
                    $category['name'] => $category['id']
                ];
            }
        )->toArray();

        $menus = config('default.menu');
        $menus = collect($menus)->map(
            function ($menu) use ($categories) {
                $menu['category_id'] = Arr::get($categories, $menu['category']);
                unset($menu['category']);

                return $menu;
            }
        )->toArray();

        $menuRepository->insertMuti($menus);

        $this->command->getOutput()->writeln( get_class($this) . '種子建立成功');
    }
}
