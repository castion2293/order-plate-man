<?php

namespace Database\Seeders;

use App\Repositories\UserRepository;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function run()
    {
        $userRepository = app()->make(UserRepository::class);
        $users = config('default.users');

        $userRepository->insertMuti($users);

        $this->command->getOutput()->writeln( get_class($this) . '種子建立成功');
    }
}
