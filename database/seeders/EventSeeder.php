<?php

namespace Database\Seeders;

use App\Repositories\EventRepository;
use Illuminate\Database\Seeder;

class EventSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function run()
    {
        $eventRepository = app()->make(EventRepository::class);

        $events = config('default.events');
        $events = collect($events)->map(
            function ($event) {
                $event['items'] = json_encode($event['items']);

                return $event;
            }
        )->toArray();

        $eventRepository->insertMuti($events);

        $this->command->getOutput()->writeln( get_class($this) . '種子建立成功');
    }
}
