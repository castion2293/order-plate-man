<?php

namespace Database\Seeders;

use App\Repositories\SpecialRepository;
use Illuminate\Database\Seeder;

class SpecialSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function run()
    {
        $specialRepository = app()->make(SpecialRepository::class);

        $specials = config('default.specials');

        $specialRepository->insertMuti($specials);

        $this->command->getOutput()->writeln( get_class($this) . '種子建立成功');
    }
}
