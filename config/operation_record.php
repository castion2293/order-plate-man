<?php

return [
    // 寫紀錄的佇列名稱
    'queue' => 'default',

    // 寫紀錄的動做行為代碼
    'action' => [
        'create' => 1,
        'update' => 2,
        'delete' => 3,
    ],

    // 功能key
    'func_keys' => [
        'update_profile' => 1001,
        'update_password' => 1002,
        'read_contact' => 1003,
    ],
];
