<?php

use Illuminate\Support\Str;

return [

    // 使用者
    'users' => [
        'name' => 'Nick',
        'email' => 'nick@example.com',
        'phone' => '0968123456',
        'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
        'profile_img' => 'profile-img.jpg',
        'company' => '雍海科技',
        'job' => '總經理',
        'country' => '台灣',
        'address' => '台中市西屯區漢口路二段151號',
        'remember_token' => Str::random(10),
    ],

    // 菜單類別預設
    'categories' => [
        'chicken',
        'burger',
        'fries',
        'drinks',
    ],

    // 菜單預設
    'menu' => [
        [
            'zh_name' => '化骨棉雞',
            'en_name' => 'Boneless Fried Chicken',
            'price' => 100,
            'type' => 2,
            'category' => 'chicken'
        ],
        [
            'zh_name' => '化骨棉雞',
            'en_name' => 'Boneless Fried Chicken',
            'price' => 150,
            'type' => 3,
            'category' => 'chicken'
        ],
        [
            'zh_name' => '美式莎莎雞',
            'en_name' => 'Salsa Sauce Fried Chicken',
            'price' => 100,
            'type' => 2,
            'category' => 'chicken'
        ],
        [
            'zh_name' => '美式莎莎雞',
            'en_name' => 'Salsa Sauce Fried Chicken',
            'price' => 150,
            'type' => 3,
            'category' => 'chicken'
        ],
        [
            'zh_name' => '墨西哥莎莎雞',
            'en_name' => 'Mexican Spicy Salsa Fried Chicken',
            'price' => 100,
            'type' => 2,
            'category' => 'chicken'
        ],
        [
            'zh_name' => '墨西哥莎莎雞',
            'en_name' => 'Mexican Spicy Salsa Fried Chicken',
            'price' => 150,
            'type' => 3,
            'category' => 'chicken'
        ],
        [
            'zh_name' => '熔岩起司雞',
            'en_name' => 'Cheese Fried Chicken',
            'price' => 100,
            'type' => 2,
            'category' => 'chicken'
        ],
        [
            'zh_name' => '熔岩起司雞',
            'en_name' => 'Cheese Fried Chicken',
            'price' => 150,
            'type' => 3,
            'category' => 'chicken'
        ],
        [
            'zh_name' => '熔岩雙起司堡',
            'en_name' => 'Double Cheese Burger',
            'price' => 130,
            'type' => 3,
            'category' => 'burger'
        ],
        [
            'zh_name' => '美式莎莎起司堡',
            'en_name' => 'Salsa Sauce Cheese Burger',
            'price' => 130,
            'type' => 3,
            'category' => 'burger'
        ],
        [
            'zh_name' => '焦糖花生起司堡',
            'en_name' => 'Caramel Peanut Butter Cheese Burger',
            'price' => 130,
            'type' => 3,
            'category' => 'burger'
        ],
        [
            'zh_name' => '熔岩起司脆薯',
            'en_name' => 'Extra Cheese French Fries',
            'price' => 80,
            'type' => 3,
            'category' => 'fries'
        ],
        [
            'zh_name' => '美式莎莎脆薯',
            'en_name' => 'Salsa Sauce French Fries',
            'price' => 80,
            'type' => 3,
            'category' => 'fries'
        ],
        [
            'zh_name' => '蜂蜜芥末脆薯',
            'en_name' => 'Honey Mustard French Fries',
            'price' => 80,
            'type' => 3,
            'category' => 'fries'
        ],
        [
            'zh_name' => '墨西哥辣莎脆薯',
            'en_name' => 'Mexican Spicy Salsa French Fries',
            'price' => 80,
            'type' => 3,
            'category' => 'fries'
        ],
        [
            'zh_name' => '經典原味脆薯',
            'en_name' => 'French Fries',
            'price' => 60,
            'type' => 3,
            'category' => 'fries'
        ],
        [
            'zh_name' => '特調梅子汁',
            'en_name' => 'Plum Juice',
            'price' => 50,
            'type' => 3,
            'category' => 'drinks'
        ],
        [
            'zh_name' => '梨山冷泡茶',
            'en_name' => 'Lishan Cold Brewed Tea',
            'price' => 50,
            'type' => 3,
            'category' => 'drinks'
        ],
        [
            'zh_name' => '氣泡飲(可樂，雪碧)',
            'en_name' => 'Soft Drink Coke or Sprite',
            'price' => 50,
            'type' => 3,
            'category' => 'drinks'
        ],
    ],

    'specials' => [
        [
            'tag' => 'tab-1',
            'name' => '大碗排骨飯',
            'title' => '１個便當搭２個配菜',
            'description' => '最常見的就是酸菜、筍絲、高麗菜跟豆干等。可以選擇炸的、或是先炸後滷的排骨，我習慣吃先炸後滷，充滿基隆在地特色！吃起來口感厚實，嚼勁十足，滷汁滋味鹹香，淋在白飯上會讓人想多扒幾口飯。除了半顆滷蛋，還有２款配菜，這次挑的是高麗菜跟酸菜，高麗菜吃起來脆甜，酸菜口感微酸甜，很配飯',
            'image' => 'img/specials-1.jpg'
        ],
        [
            'tag' => 'tab-2',
            'name' => '水果拼盤',
            'title' => '把鮪魚玉米罐頭分別放在兩片吐司上灑上肉鬆即可',
            'description' => '番茄、水果切碎放在盤子、上煎蛋兩顆灑上一點調味擺上、倒兩杯飲料放在旁邊、就完成視覺上很厲害又簡單到爆的早餐拼盤囉',
            'image' => 'img/specials-2.jpg'
        ],
        [
            'tag' => 'tab-3',
            'name' => '大碗牛肉麵',
            'title' => '表面鋪著滿滿的牛腱，幾乎看不到底下的麵！',
            'description' => '碗裡是切成薄片的牛腱肉，肉質軟嫩適口，還能吃到微微的膠質；重點是牛腱份量真的給得很大方，魚編突然很慶幸自己點的是小碗，因為光是小碗就吃不完了啊',
            'image' => 'img/specials-3.jpg'
        ],
        [
            'tag' => 'tab-4',
            'name' => '奶酥拼盤',
            'title' => '可口好吃的巧克力奶酥麵包，是早餐和下午茶的好選擇',
            'description' => '自製奶酥醬超級簡單～零廚藝的人肯定也能輕鬆完成！如果喜歡口味豐富一點的人～也可以自行加入椰絲或葡萄乾來增加口感！只是奶酥醬放冰箱冷藏保存容易變硬～製作完後最好盡快食用完畢唷',
            'image' => 'img/specials-4.jpg'
        ],
        [
            'tag' => 'tab-5',
            'name' => '水果輕食',
            'title' => '夏日炎炎，食用香甜可口的水果幫助消暑，是許多民眾非常喜愛的方式',
            'description' => '吃水果可有相當大的學問，究竟怎麼吃才能享受水果的美味又不發胖呢？事實上，只要仔細挑選，吃水果不僅能解暑，還有助於瘦身。例如，蘋果、檸檬、鳳梨、香蕉等，就是減重民眾想要食用水果食的不錯選擇',
            'image' => 'img/specials-5.jpg'
        ],
    ],

    'events' => [
        [
            'image' => 'img/event-birthday.jpg',
            'name' => '生日派對',
            'price' => 20000,
            'title' => '想像一下世界上有一種派對，是空氣中瀰漫著歡樂美好的甜蜜香味，最棒的是此刻幸福的環境和氛圍，完完全全是由為人父母親的您親手打造而出的！ 每個人生命中摯愛的時刻，不就只是追求這般與家人、朋友間這種共聚的獨特幸福時光！',
            'items' => [
                '到府佈置',
                '禮品小物專區',
                '生日派對懶人包',
                '泡泡先生',
                'Rody跳跳馬'
            ]
        ],
        [
            'image' => 'img/event-private.jpg',
            'name' => '私人派對',
            'price' => 15000,
            'title' => '由你當主人，一同與親朋好友們歡度愉快的時光，我們擁有專業的DJ團隊，企劃、管理、公關與設計皆一手包辦。派對種類各式各樣，依照地點與進行方式的不同，打造出屬於你個人的專屬派對',
            'items' => [
                '獨立包廂設計，盡情享受派對',
                '頂級音響設備',
                'KTV歡唱',
                '藍光DVD播放器',
                '部分酒精飲料'
            ]
        ],
        [
            'image' => 'img/event-custom.jpg',
            'name' => '迎賓派對',
            'price' => 24000,
            'title' => '一場婚禮中，除了大家最為熟悉的婚宴(囍酒)以外，也有在婚宴開始前的迎賓酒會與婚宴後的After Party。迎賓酒會算是婚宴的暖身，讓賓客們可先享用幾樣簡單的小點及飲品招待，等待正式婚宴的開始',
            'items' => [
                '喜帖賀卡',
                '糖果餅乾',
                '彩色氣球',
                '鮮花佈置',
                '白色桌巾'
            ]
        ],
    ]
];
