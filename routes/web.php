<?php

use App\Http\Controllers\Web\ContactController;
use App\Http\Controllers\Web\EventController;
use App\Http\Controllers\Web\GalleryController;
use App\Http\Controllers\Web\MenuController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::view('/', 'web.home')->name('home');
Route::view('/about', 'web.about')->name('about');
Route::get('/menu', [MenuController::class, 'lists'])->name('menu');
Route::get('/events', [EventController::class, 'lists'])->name('events');
Route::get('/gallery', [GalleryController::class, 'images'])->name('gallery');
Route::get('/contact', [ContactController::class, 'index'])->name('contact.index');
Route::post('/contact', [ContactController::class, 'store'])->name('contact.store');
