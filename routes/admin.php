<?php

use App\Http\Controllers\Admin\ContactController;
use App\Http\Controllers\Admin\FaqController;
use App\Http\Controllers\Admin\SystemController;
use App\Http\Controllers\Admin\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Admin Routes
|--------------------------------------------------------------------------
|
*/

Route::middleware('guest')->group(function () {
    Route::get('login', [SystemController::class, 'index'])->name('login.index');
    Route::post('login', [SystemController::class, 'login'])->name('login');
});

Route::middleware('auth:admin')->group(function () {
    Route::post('logout', [SystemController::class, 'logout'])->name('logout');

    Route::put('user/update-password', [UserController::class, 'updatePassword'])->name('admin.user.update_password');
    Route::resource('user', UserController::class)->only(['index', 'update'])->names('admin.user');

    Route::get('faq', [FaqController::class, 'index'])->name('admin.faq');

    Route::get('home', [SystemController::class, 'home'])->name('admin.home');

    Route::get('contact', [ContactController::class, 'index'])->name('admin.contact.index');
    Route::get('contact/{id}', [ContactController::class, 'show'])->name('admin.contact.show');
});


