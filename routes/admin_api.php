<?php

use App\Http\Controllers\Admin\OperationRecordController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//    return $request->user();
//});

Route::middleware('auth:admin')->group(function () {
    Route::prefix('home')->group(function () {
        Route::get('sales', function () {
            return [
                'today' => 200,
                'week' => 1000,
                'month' => 25000
            ];
        })->name('api.admin.home.sales');

        Route::get('revenues', function () {
            return [
                'today' => 20000,
                'week' => 100000,
                'month' => 2500000
            ];
        })->name('api.admin.home.revenues');

        Route::get('customers', function () {
            return [
                'today' => 45,
                'week' => 138,
                'month' => 1244
            ];
        })->name('api.admin.home.customers');

        Route::get('reports', function () {
            return [
                'series' => [
                    [
                        'name' => 'sales',
                        'data' => Arr::shuffle([31, 40, 28, 51, 42, 82, 56]),
                    ],
                    [
                        'name' => 'revenues',
                        'data' => Arr::shuffle([11, 32, 45, 32, 34, 52, 41]),
                    ],
                    [
                        'name' => 'customers',
                        'data' => Arr::shuffle([15, 11, 32, 18, 9, 24, 11]),
                    ],
                ],
                'categories' => [
                    '2022-01-10 00:00:00',
                    '2022-01-10 01:00:00',
                    '2022-01-10 02:00:00',
                    '2022-01-10 03:00:00',
                    '2022-01-10 04:00:00',
                    '2022-01-10 05:00:00',
                    '2022-01-10 06:00:00',
                ],
            ];
        })->name('api.admin.home.reports');

        // 操作記錄
        Route::get('operation-records', [OperationRecordController::class, 'index'])->name('api.admin.home.operation_record');
    });
});


